package lab;

public class Rectangle {
	int sideA;
	int sideB;
	Point topLeft;
	
	public Rectangle(int sideA, int sideB, Point topLeft) {
		this.sideA = sideA;
		this.sideB = sideB;
		this.topLeft = topLeft;
	}
	public int area() {
		return this.sideA * this.sideB;
	}
	public int perimeter() {
		return 2*(this.sideA + this.sideB);
	}
	
	public Point[] corners() {
		Point bottomLeft = new Point(this.topLeft.xCoord, this.topLeft.yCoord-sideA);
		Point topRight =  new Point(this.topLeft.xCoord + sideB,this.topLeft.yCoord);
		Point bottomright = new Point(this.topLeft.xCoord + sideB,this.topLeft.yCoord - sideA);
	
		Point[] pointArray = new Point[4];
		pointArray[0] = this.topLeft;
		pointArray[1] = bottomLeft;
		pointArray[2] = topRight;
		pointArray[3] = bottomright;
		
		return pointArray;
	}
}
