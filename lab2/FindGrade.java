public class FindGrade {
    
    public static void main (String[] args){
        int value= Integer.parseInt(args[0]);

        if ((value > 100) || (value < 0)) {
            System.out.println("Enter a valid value!");
        } else if (value >= 90) {
            System.out.println("A");
        } else if (value >= 80) {
            System.out.println("B");
        } else if (value >= 70) {
            System.out.println("C");
        } else if (value >= 60) {
            System.out.println("D");
        } else {
            System.out.println("F"); 
        }
    
    }
}